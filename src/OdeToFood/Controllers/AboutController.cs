﻿using Microsoft.AspNetCore.Mvc;

namespace OdeToFood.Controllers
{
    //[Route("about")]
    [Route("[controller]/[action]")]
    public class AboutController
    {
        //[Route("phone")]
        [Route("[action]")]
        public string Phone()
        {
            return "1+555-555-555";
        }

        [Route("address")]
        public string Address()
        {
            return "USA";
        }
    }
}
