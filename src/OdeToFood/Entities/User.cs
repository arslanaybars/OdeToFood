﻿
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace OdeToFood.Entities
{
    public class User : IdentityUser
    {
        public User()
        {
            // ... can add new properties for customize
        }
    }
}
